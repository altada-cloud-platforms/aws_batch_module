resource "aws_batch_compute_environment" "sample" {
  compute_environment_name = var.ce_name

  compute_resources {
    instance_role = aws_iam_instance_profile.ecs_instance_role.arn
    instance_type = [
      "optimal",
    ]
    max_vcpus = var.max_vcpu
    min_vcpus = var.min_vcpu
    desired_vcpus = var.desired_vcpu
    security_group_ids = var.security_group_id
    ec2_key_pair = var.ec2_key_pair
    subnets = var.subnet_id
    type = "EC2"
    allocation_strategy = "BEST_FIT"
  }

  service_role = aws_iam_role.aws_batch_service_role.arn
  type         = var.ce_type
  depends_on   = [aws_iam_role_policy_attachment.aws_batch_service_role]
}

resource "aws_batch_job_queue" "test_queue" {
  name     = var.batch_job_queue_name
  state    = var.batch_job_queue_state
  priority = var.batch_job_queue_priority
  compute_environments = [
    aws_batch_compute_environment.sample.arn

  ]
}

resource "aws_batch_job_queue" "test_queue1" {
  name     = var.batch_job_queue_name1
  state    = var.batch_job_queue_state
  priority = var.batch_job_queue_priority
  compute_environments = [
    aws_batch_compute_environment.sample.arn

  ]
}
resource "aws_batch_job_queue" "test_queue2" {
  name     = var.batch_job_queue_name2
  state    = var.batch_job_queue_state
  priority = var.batch_job_queue_priority
  compute_environments = [
    aws_batch_compute_environment.sample.arn

  ]
}

resource "aws_batch_job_queue" "test_queue3" {
  name     = var.batch_job_queue_name3
  state    = var.batch_job_queue_state
  priority = var.batch_job_queue_priority
  compute_environments = [
    aws_batch_compute_environment.sample.arn

  ]
}

resource "aws_batch_job_definition" "job-definition" {
  name                 = var.batch_job_definition_name
  type                 = "container"
  platform_capabilities = ["EC2"]
  timeout              {
    attempt_duration_seconds  = var.timeout
  }
  container_properties = var.ecs_container_properties
}
