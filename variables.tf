variable "instance_type" {
  description = "The instance_type for compute environment to use."
  type        = string
  default     = "optimal"
}

variable "ce_type" {
  description = "Compute Environment type."
  type        = string
  default     = "MANAGED"
}

variable "ce_name" {
  description = "Given name for the Compute Environment."
  type        = string
  default = "test-compute-engine"
}

variable "max_vcpu" {
  description = "Maximum allowed VCPUs allocated to instances."
  type        = number
  default     = 32
}

variable "min_vcpu" {
  description = "Minimum number of VCPUs allocated to instances."
  type        = number
  default     = 0
}

variable "desired_vcpu" {
  description = "Desired number of VCPUs allocated to instances."
  type        = number
  default     = 0
}

variable "ce_allocation_strategy" {
  description = "The allocation strategy to use for the compute resource."
  default     = "BEST_FIT"
}

variable "batch_job_queue_name" {
  description = "Batch Job queue name."
  type        = string
  default     = "batch-job-queue"
}

variable "batch_job_queue_name1" {
  description = "Batch Job queue name."
  type        = string
  default     = "batch-job-queue1"
}

variable "batch_job_queue_name2" {
  description = "Batch Job queue name."
  type        = string
  default     = "batch-job-queue2"
}

variable "batch_job_queue_name3" {
  description = "Batch Job queue name."
  type        = string
  default     = "batch-job-queue3"
}


variable "batch_job_queue_state" {
  description = "State of the created Job Queue."
  type        = string
  default     = "ENABLED"
}

variable "batch_job_queue_priority" {
  description = "Priority of the created Job Queue."
  type        = string
  default     = "1"
}

variable "batch_job_definition_name" {
  description = "Batch Job definition name."
  type        = string
  default     = "batch-job-definition"
}

variable "subnet_id" {
    description = "Subnet id for Batch"
    type = list(string)
    default = []
  
}

variable "security_group_id" {
    description = "Security Group id for Batch"
    type = list(string)
    default = []
  
}

variable "ecs_container_properties" {
  description = "A valid container properties provided as a single valid JSON document."
  default = ""
}


variable "timeout"{
  description = "timeout job-definition"
  default = "1800"
}

variable "policy" {
  description = "Name of the policy"
  type = string
  default = "EKS_EFS_CSI_DRIVER_POLICY"
  
}

variable "ec2_key_pair" {
  description = "Name of the key pair"
  type = string
  default = ""
}
