Content-Type: multipart/mixed; boundary="==BOUNDARY==" 
MIME-Version: 1.0 


--==BOUNDARY== 
MIME-Version: 1.0 
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash 
sudo yum install -y amazon-efs-utils
sudo pip3 install botocore --upgrade
echo ECS_CLUSTER=OCR-Version-three_Batch_e9054a06-81b9-3b6e-b1da-2a2d60c9af6b>>/etc/ecs/ecs.config 
echo ECS_DISABLE_IMAGE_CLEANUP=false>>/etc/ecs/ecs.config 
echo ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=2m>>/etc/ecs/ecs.config 
echo ECS_IMAGE_CLEANUP_INTERVAL=10m>>/etc/ecs/ecs.config 
echo ECS_IMAGE_MINIMUM_CLEANUP_AGE=10m>>/etc/ecs/ecs.config 
echo ECS_NUM_IMAGES_DELETE_PER_CYCLE=5>>/etc/ecs/ecs.config
echo ECS_RESERVED_MEMORY=32>>/etc/ecs/ecs.config
echo ECS_INSTANCE_ATTRIBUTES={\"com.amazonaws.batch.compute-environment-revision\":\"0\"}>>/etc/ecs/ecs.config

--==BOUNDARY==--
